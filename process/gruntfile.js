module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

  express: {
    options: {
      // Override defaults here
    },
    dev: {
      options: {
        script: 'server.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-express-server');

  // Default task(s).
  grunt.registerTask('default', [ 'express:dev']);

};