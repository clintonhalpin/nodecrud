var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('itemsdb', server, {safe: true});

db.open(function(err, db) {
   
   // Clear if you want to... db.collection('listitems').drop();

    if(!err) {
        console.log("Connected to 'itemsdb' database");
        db.collection('listitems', {safe:true}, function(err, collection) {
            if (err) {
                console.log("The 'items' collection doesn't exist. Creating it with sample data...");
                populateDB();
            }
        });
    }
});

exports.findAll = function(req, res) {
    db.collection('listitems', function(err, collection) {
        collection.find().toArray(function(err, items) {
           res.send(items);
        });
    });
};

exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving Item: ' + id);
    db.collection('listitems', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};


exports.deleteItem = function(req, res) {
    var id = req.params.id;
    console.log('Deleting item: ' + id);
    db.collection('listitems', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}

var populateDB = function() {

    var listitems = [
    {
        name: "Pizza"
    },
    {
        name: "Yooooo"
    }];

    db.collection('listitems', function(err, collection) {
        collection.insert(listitems, {safe:true}, function(err, result) {});
    });

};