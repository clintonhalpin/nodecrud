# Basic Crud App with Node :)

A simple application for creating a restful service using Node, Express, MongoDB. Alot of this code is based on this article. http://coenraets.org/blog/2012/10/creating-a-rest-api-using-node-js-express-and-mongodb/

# Get Started 

// Installs all packages

> npm install

// Fire up MongoDB

> mongod

// Run the app with Supervisor https://github.com/isaacs/node-supervisor

> supervisor server.js

// Test Available Routes

> curl -i -X GET http://localhost:3000/items

> curl -i -X GET http://localhost:3000/items/:id

> curl -i -X  DELETE http://localhost:3000/items/:id




